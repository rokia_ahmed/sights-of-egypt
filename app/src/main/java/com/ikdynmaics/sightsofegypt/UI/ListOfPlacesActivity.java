package com.ikdynmaics.sightsofegypt.UI;

import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ikdynmaics.sightsofegypt.R;
import com.ikdynmaics.sightsofegypt.adapter.PlacesAdapter;
import com.ikdynmaics.sightsofegypt.helper.ConnectionDetector;
import com.ikdynmaics.sightsofegypt.helper.ConnectivityReceiver;
import com.ikdynmaics.sightsofegypt.model.Place;
import com.ikdynmaics.sightsofegypt.rest.ApiClient;
import com.ikdynmaics.sightsofegypt.rest.ApiInterface;
import com.vlonjatg.progressactivity.ProgressFrameLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListOfPlacesActivity extends AppCompatActivity {

    private static ApiInterface apiService;
    private static RecyclerView recyclerView;
    private static PlacesAdapter placeAdapter;
    private String TAG = ListOfPlacesActivity.class.getSimpleName();
    private static View footerView;
    private boolean isLoading = false;
    private int pageIndex = 0;
    private static String MY_SHARED = "places_shared";
    private static String MY_SHARED_PLACES_LIST = "places_list_shared";
    private static String PAGE_INDEX = "page_index";
    private static ProgressFrameLayout progressFrameLayout;
    private ConnectionDetector connectionDetector;
    private static List<Place> emptyList;
    private static SharedPreferences placesPrefs;

    private final View.OnClickListener getErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (connectionDetector.isConnectingToInternet()) {
                loadPlacesList(pageIndex);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_places);

        placesPrefs = getSharedPreferences(MY_SHARED, Context.MODE_PRIVATE);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_list_of_places);
        recyclerView.setHasFixedSize(true);
        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        emptyList = new ArrayList<>();
        connectionDetector = new ConnectionDetector(this);
        progressFrameLayout = (ProgressFrameLayout) findViewById(R.id.progress_frame_layout);

        // adapter
        placeAdapter = new PlacesAdapter(ListOfPlacesActivity.this, emptyList);
        recyclerView.setAdapter(placeAdapter);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        // call shared
        SharedPreferences placesPrefs = getSharedPreferences(MY_SHARED, Context.MODE_PRIVATE);
        pageIndex = placesPrefs.getInt(PAGE_INDEX, 0);
        String oldItemsShared = placesPrefs.getString(MY_SHARED_PLACES_LIST, null);
        if (oldItemsShared != null) {
            Gson gson = new Gson();
            ArrayList<Place> oldItems = new ArrayList<>(Arrays.asList(gson.fromJson(oldItemsShared, Place[].class)));
            placeAdapter.addItems(oldItems);
            placeAdapter.notifyDataSetChanged();
        } else {
            // call API
            if (connectionDetector.isConnectingToInternet()) {
                loadPlacesList(pageIndex);
            } else {
                progressFrameLayout.showError(ContextCompat.getDrawable(this, R.drawable.ic_wifi_off__24dp), "",
                        "",
                        getString(R.string.try_again), getErrorClickListener);
            }
        }

        footerView = getLayoutInflater().inflate(R.layout.footer_load_more_layout, null);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount, firstVisibleItem = 0;

                totalItemCount = staggeredGridLayoutManager.getItemCount();
                int[] firstVisibleItemsList = null;
                firstVisibleItemsList = staggeredGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItemsList);
                if (firstVisibleItemsList != null && firstVisibleItemsList.length > 0) {
                    firstVisibleItem = firstVisibleItemsList[0];
                }

                handlePaging(visibleItemCount, totalItemCount, firstVisibleItem);
            }
        });

    }

    private void handlePaging(int visibleItemCount, int totalItemCount, int firstVisibleItem) {
        int lastIndexInScreen = visibleItemCount + firstVisibleItem;
        Log.i("visibleItemCount", visibleItemCount + "");
        Log.i("firstVisibleItem", firstVisibleItem + "");
        Log.i("lastIndexInScreen", firstVisibleItem + "");
        if (lastIndexInScreen >= totalItemCount && !isLoading) {
            // It is time to load more items
            isLoading = true;
            loadMore();
        }
    }


    private void loadMore() {
        if (connectionDetector.isConnectingToInternet()) {
            loadPlacesList(pageIndex);
        } else {
            isLoading = false;
            Toast.makeText(getApplicationContext(), R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    public void updateList(boolean isConnected, Context context) {
        if (isConnected) {
            isLoading = true;
            pageIndex = 0;
            SharedPreferences.Editor placesEditor = placesPrefs.edit();
            placesEditor.putString(MY_SHARED_PLACES_LIST, null);
            placesEditor.apply();
            loadPlacesList(0);
        }
    }

    private void loadPlacesList(int index) {
        placeAdapter.addFooter(footerView);
        if (!isLoading) {
            progressFrameLayout.showLoading();
        }
        Call<List<Place>> callAllPlaces = apiService.getAllPlacesInEgypt(ApiClient.BASE_COUNT, index);
        callAllPlaces.enqueue(new Callback<List<Place>>() {
            @Override
            public void onResponse(Call<List<Place>> call, Response<List<Place>> response) {
                int status = response.code();
                if (status == 200) {
                    placeAdapter.removeFooter(footerView);
                    progressFrameLayout.showContent();
                    List<Place> newItems = response.body();
                    Log.d("TAG", "Page Index: " + pageIndex);
                    Gson gson = new Gson();
                    String oldItemsShared = placesPrefs.getString(MY_SHARED_PLACES_LIST, null);
                    SharedPreferences.Editor placesEditor = placesPrefs.edit();
                    if (oldItemsShared != null) {
                        ArrayList<Place> oldItems = new ArrayList<>(Arrays.asList(gson.fromJson(oldItemsShared, Place[].class)));
                        oldItems.addAll(newItems);
                        placesEditor.putString(MY_SHARED_PLACES_LIST, gson.toJson(oldItems));
                    } else {
                        placesEditor.putString(MY_SHARED_PLACES_LIST, gson.toJson(newItems));
                    }
                    pageIndex += ApiClient.BASE_COUNT;
                    isLoading = false;
                    placesEditor.putInt(PAGE_INDEX, pageIndex);
                    placesEditor.apply();
                    placeAdapter.addItems(newItems);
                    placeAdapter.notifyDataSetChanged();
                } else if (status == 0) {
                    progressFrameLayout.showError(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_wifi_off__24dp), "",
                            "",
                            getString(R.string.try_again), getErrorClickListener);
                } else if (status == 500) {
                    Toast.makeText(getApplicationContext(), R.string.server_error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Place>> call, Throwable t) {
                Log.d(TAG, "Error loading list: " + t.getMessage());
            }
        });
    }
}
