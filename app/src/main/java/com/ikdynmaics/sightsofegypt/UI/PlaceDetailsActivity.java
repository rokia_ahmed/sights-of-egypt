package com.ikdynmaics.sightsofegypt.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ikdynmaics.sightsofegypt.R;
import com.ikdynmaics.sightsofegypt.model.Place;
import com.squareup.picasso.Picasso;

public class PlaceDetailsActivity extends AppCompatActivity {

    private ImageView placeImageIV;
    private TextView priceTV;
    private TextView descriptionTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);

        placeImageIV = (ImageView) findViewById(R.id.image_view_place_pic);
        priceTV = (TextView) findViewById(R.id.text_view_price);
        descriptionTV = (TextView) findViewById(R.id.text_view_description);

        if (getIntent().getExtras() != null) {
            Place placeObject = (Place) getIntent().getExtras().getSerializable("place_object");
            priceTV.setText(String.valueOf(placeObject.getPrice()) + " $");
            descriptionTV.setText(String.valueOf(placeObject.getProductDescription()));
            Picasso.with(this).load(placeObject.getImage().getUrl()).into(placeImageIV);
        }

    }

    public void onClick(View v){
        finish();
    }
}
