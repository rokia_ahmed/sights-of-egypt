package com.ikdynmaics.sightsofegypt.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ikdynmaics.sightsofegypt.R;
import com.ikdynmaics.sightsofegypt.UI.PlaceDetailsActivity;
import com.ikdynmaics.sightsofegypt.model.Place;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rokia on 06/08/17.
 */

public class PlacesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Place> mListOfPlaces;
    private final List<View> footers = new ArrayList<>();
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;


    public PlacesAdapter(Context context, List<Place> items) {
        mContext = context;
        mListOfPlaces = items;
    }

    @Override
    public RecyclerView.ViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_place, parent, false);
            return new ViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.footer_load_more_layout, parent, false);
            return new HeaderFooterViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder mHolder = (ViewHolder) holder;
        mHolder.priceTV.setText(String.valueOf(mListOfPlaces.get(position).getPrice()) + "$");
        mHolder.descriptionTV.setText(String.valueOf(mListOfPlaces.get(position).getProductDescription()));
//        mHolder.itemFL.getLayoutParams().width = mListOfPlaces.get(position).getImage().getWidth();
        mHolder.placeImageIV.getLayoutParams().height = mListOfPlaces.get(position).getImage().getHeight();
        String imageURL = mListOfPlaces.get(position).getImage().getUrl();
        Picasso.with(mContext).load(imageURL).into(mHolder.placeImageIV);
    }

    @Override
    public int getItemViewType(int position) {
        if (position >= mListOfPlaces.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mListOfPlaces.size();
    }

    public void addItems(List<Place> placesList) {
        mListOfPlaces.addAll(placesList);
    }

    //add a footer to the adapter
    public void addFooter(View footer) {
        if (!footers.contains(footer)) {
            footers.add(footer);
            //animate
            notifyItemInserted(mListOfPlaces.size() + footers.size() - 1);
        }
    }

    //remove a footer from the adapter
    public void removeFooter(View footer) {
        if (footers.contains(footer)) {
            //animate
            notifyItemRemoved(mListOfPlaces.size() + footers.indexOf(footer));
            footers.remove(footer);
            if (footer.getParent() != null) {
                ((ViewGroup) footer.getParent()).removeView(footer);
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView placeImageIV;
        private TextView priceTV;
        private TextView descriptionTV;
        private ProgressBar progressBar;


        public ViewHolder(final View itemView) {
            super(itemView);

            placeImageIV = (ImageView) itemView.findViewById(R.id.image_view_place_pic);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
            priceTV = (TextView) itemView.findViewById(R.id.text_view_price);
            descriptionTV = (TextView) itemView.findViewById(R.id.text_view_description);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getLayoutPosition();
                    Intent intent = new Intent(mContext, PlaceDetailsActivity.class);
                    intent.putExtra("place_object", mListOfPlaces.get(position));
                    mContext.startActivity(intent);
                }
            });
        }
    }

    //our footer RecyclerView.ViewHolder is just a FrameLayout
    public static class HeaderFooterViewHolder extends RecyclerView.ViewHolder {
         public HeaderFooterViewHolder(View itemView) {
            super(itemView);
         }
    }

}
