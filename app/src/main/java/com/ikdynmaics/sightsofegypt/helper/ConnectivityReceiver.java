package com.ikdynmaics.sightsofegypt.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ikdynmaics.sightsofegypt.R;
import com.ikdynmaics.sightsofegypt.UI.ListOfPlacesActivity;
import com.ikdynmaics.sightsofegypt.model.Place;
import com.ikdynmaics.sightsofegypt.rest.ApiClient;
import com.ikdynmaics.sightsofegypt.rest.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rokia on 08/08/17.
 */

public class ConnectivityReceiver  extends BroadcastReceiver {
    private ConnectionDetector connectionDetector;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("onReceive", "run");
        connectionDetector = new ConnectionDetector(context);

        if (connectionDetector.isConnectingToInternet()) {
            new ListOfPlacesActivity().updateList(true, context);
         }
    }

}
