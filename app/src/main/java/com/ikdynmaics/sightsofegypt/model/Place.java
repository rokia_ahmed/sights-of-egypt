package com.ikdynmaics.sightsofegypt.model;

import java.io.Serializable;

/**
 * Created by rokia on 06/08/17.
 */

public class Place implements Serializable{
    private double id;
    private String productDescription;
    private ImageInfo image;
    private int price;

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public ImageInfo getImage() {
        return image;
    }

    public void setImage(ImageInfo image) {
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
