package com.ikdynmaics.sightsofegypt.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rokia on 06/08/17.
 */

public class ApiClient {

    public static final String BASE_URL = "http://sightsofegypt.getsandbox.com/";
    public static final int BASE_COUNT = 10;
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
