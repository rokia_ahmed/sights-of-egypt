package com.ikdynmaics.sightsofegypt.rest;

import com.ikdynmaics.sightsofegypt.model.Place;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rokia on 06/08/17.
 */

public interface ApiInterface {

    @GET("explore")
    Call<List<Place>> getAllPlacesInEgypt(@Query("count") int count, @Query("from") int from);
}
